<?php
    namespace Zimplify\Push\Providers;
    use Zimplify\Core\{Application, Provider};
    use Zimplify\Core\Interfaces\{IObjectAuthorInterface, IMessageDispatchableInterface};
    use \RuntimeException;
    
    /**
     * <provide description for this service provider>
     * @package Zimplify\Push (code 03)
     * @type Provider (code 03)
     * @file PushProvider (code 01)
     */
    class PushProvider extends Provider implements IMessageDispatchableInterface {
    
        const CFG_PUSH_ENGINE = "system.providers.push.engine";
        const DEF_CLS_NAME = self::class;
        const DEF_SHT_NAME = "core-push::push";
            
        /**
         * get all details on the attachments that will add to email
         * @param Message $message the message to send
         * @return array
         */
        protected function getAttachments(Message $message) : array {
            $result = [];
            foreach ($message->getAttachments() as $attachment) {
                $file = $attachment->mime."::".$attachment->location;
                array_push($result, $file);
            }
            return $result;
        }

        /**
         * initializing the provider.
         * @return void
         */
        protected function initialize() {
            $engine = Application::env(self::CFG_EMAIL_ENGINE);
            $this->engine = Application::request($engine, []);
        }
    
        /**
         * check if all required arguments are supplied into the provider for initialization.
         * @return bool
         */
        protected function isRequired() : bool {
            return true;
        }

        /**
         * sending out the message via the carrier
         * @param Message $message the message to send
         * @return bool
         */
        public function send(Message $message) : bool {
            return $this->engine->send(
                $this->parent()->id,
                $this->{Message::FLD_RECIPIENTS},
                $message->body, 
                $message->title, 
                $this->getAttachments($message));
        }
    }