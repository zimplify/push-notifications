<?php
    namespace Zimplify\Push;
    use Zimplify\Core\{Application, Message, Query};
    use Zimplify\Core\Interfaces\IMessagingAgentInterface;
    use Zimplify\Push\Providers\PushProvider;
    use \RuntimeException;

    /**
     * the PushNotification is the core message container for the message to be send to others
     * @package Zimplify\Push (code 03)
     * @type instance (code 01)
     * @file PushNotification (code 01)
     */
    class PushNotification extends Message {
        // our class constants
        const DEF_CLS_NAME = self::class;
        const DEF_MSG_TYPE = "push";
        const DEF_SHT_NAME = "core-push::push-notification";

        /**
         * getting the carrier for the message
         * @return Provider
         */
        protected function getCarrier() : Provider {
            $adapter = Application::request(PushProvider::DEF_SHT_NAME, []);
            if (is_a($adapter, IMessagingAgentInterface::class)) {
                return $adapter;
            } else 
                throw new RuntimeException("Provider is not compliant.", self::ERR_NOT_COMPLIANT);
        }
        
        /**
         * get the template type for the message during render
         * @return string
         */
        protected function getMessageType() : string {
            return self::DEF_MSG_TYPE;
        }     
        
        /**
         * our instance initialization routine (like envokes, etc)
         * @return void
         */
        protected function prepare() {
        }
    }